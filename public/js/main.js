function GameDetails(servername, serverurl, mapname, maxplayers, steamid, gamemode, volume, language) {
    $("#map").append(mapname);
    $("#gamemode").append(gamemode);
    $("#maxplayers").append(maxplayers);
    document.getElementById("audio").volume = volume;

    //$("#name").append(NAME);
    //let api = "https://steamcommunity.com/profiles/" + steamid + "/?xml=1";
}

// For testing
/*
GameDetails(
    "[TAGS] Oculus - DrkRP [TAGS]",
    "https://commandcracker.gitlab.io/ogm/",
    "cs_office",
    "?/20",
    "76561198402441709",
    "darkrp",
    "0.54",
    "language"
);
*/
